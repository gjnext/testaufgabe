<?php


namespace ssi\entity\match;


class MatchModel
{

    /** @var int $matchId */
    private int $matchId;

    /** @var string $date */
    private string $date;

    /** @var int $sportId */
    private int $sportId;

    /** @var string $sportType */
    private string $sportType;

    /** @var int $cityId */
    private int $cityId;

    /** @var string $cityName */
    private string $cityName;

    /** @var string $teamNames */
    private string $teamNames;

    /**
     * @return int
     */
    public function getMatchId(): int
    {
        return $this->matchId;
    }

    /**
     * @param int $matchId
     */
    public function setMatchId(int $matchId): void
    {
        $this->matchId = $matchId;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getSportId(): int
    {
        return $this->sportId;
    }

    /**
     * @param int $sportId
     */
    public function setSportId(int $sportId): void
    {
        $this->sportId = $sportId;
    }

    /**
     * @return string
     */
    public function getSportType(): string
    {
        return $this->sportType;
    }

    /**
     * @param string $sportType
     */
    public function setSportType(string $sportType): void
    {
        $this->sportType = $sportType;
    }

    /**
     * @return int
     */
    public function getCityId(): int
    {
        return $this->cityId;
    }

    /**
     * @param int $cityId
     */
    public function setCityId(int $cityId): void
    {
        $this->cityId = $cityId;
    }

    /**
     * @return string
     */
    public function getCityName(): string
    {
        return $this->cityName;
    }

    /**
     * @param string $cityName
     */
    public function setCityName(string $cityName): void
    {
        $this->cityName = $cityName;
    }

    /**
     * @return string
     */
    public function getTeamNames(): string
    {
        return $this->teamNames;
    }

    /**
     * @param string $teamNames
     */
    public function setTeamNames(string $teamNames): void
    {
        $this->teamNames = $teamNames;
    }
}
