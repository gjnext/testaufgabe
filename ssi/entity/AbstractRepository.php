<?php


namespace ssi\entity;

use DB;

class AbstractRepository implements IAbstractRepository
{
    const ENTITY = '';
    public const MAX_ITEM_IN_QUERY = 2;

    /**
     * Zeigt wie viele Seiten es gibt
     *
     * @param string $entity
     * @return int
     */
    public static function getPagesCount(string $entity): int
    {
        /** @var int $paginationCount wie viele Zeilen gibts in der Tabelle */
        $paginationCount = DB::select(sprintf('SELECT COUNT(*) FROM `%s`', $entity))[0][0];

        /** @var int $pages auf wie viele Seiten können wir die Anfrage aufteilen */
        return ceil($paginationCount / self::MAX_ITEM_IN_QUERY);
    }

    /**
     * @param int $teamId
     * @return string
     */
    public static function getTeamNameById(int $teamId): string
    {
        $query = '
            SELECT `team_name`
            FROM `teams`
            WHERE `id` = :TEAM_ID
        ';

        return (string)DB::select($query, ['TEAM_ID' => $teamId])[0]['team_name'];
    }

    /**
     * @param int $cityId
     * @return mixed
     */
    public static function getCityNameById(int $cityId): int
    {
        $query = '
            SELECT `city_name`
            FROM `cities`
            WHERE `id` = :CITY_ID
        ';

        return (int)DB::select($query, ['CITY_ID' => $cityId])[0]['city_name'];
    }

    /**
     * @param string $cityId
     * @return string
     */
    public static function getSportTypeById(string $cityId): string
    {
        $query = '
            SELECT `sports_name`
            FROM `sports`
            WHERE `id` = :SPORTS_ID
        ';

        return (string)DB::select($query, ['SPORTS_ID' => $cityId])[0]['sports_name'];
    }
}
