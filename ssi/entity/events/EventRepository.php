<?php


namespace ssi\entity\events;

use DB;
use ssi\entity\AbstractRepository;

class EventRepository extends AbstractRepository
{

    public const ENTITY = 'events';


    /**
     * @return array
     */
    public static function getAll(): array
    {
        /** @var int $page welche Seite wurde angefragt */
        //$page = isset($request->get('page')) ? (int)$request->get('page') : 1;
        $page = 0;

        $pagesCount = self::getPagesCount(self::ENTITY);

        $events = [];

        $i = 1;
        while ($i <= $pagesCount) {
            $offset = $page * self::MAX_ITEM_IN_QUERY;

            $query = sprintf(
                'SELECT `id`, `match_id`, `sport_id`, `team_id`, `city_id`, `date`
                FROM `events`
                ORDER BY `match_id`, date ASC LIMIT %s OFFSET %s',
                self::MAX_ITEM_IN_QUERY,
                $offset
            );

            $events[] = DB::select($query);

            $i++;
            $page++;
        }

        return $events;
    }
}
