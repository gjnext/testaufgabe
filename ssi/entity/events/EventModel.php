<?php


namespace ssi\entity\events;


class EventModel
{

    /** @var int $id */
    private int $id;

    /** @var int $matchId */
    private int $matchId;

    /** @var int $sportId */
    private int $sportId;

    /** @var int $teamId */
    private int $teamId;

    /** @var int $cityId */
    private int $cityId;

    /** @var string $date */
    private string $date;

    /** @var string $teamName */
    private string $teamName;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getMatchId(): int
    {
        return $this->matchId;
    }

    /**
     * @param int $matchId
     */
    public function setMatchId(int $matchId): void
    {
        $this->matchId = $matchId;
    }

    /**
     * @return int
     */
    public function getSportId(): int
    {
        return $this->sportId;
    }

    /**
     * @param int $sportId
     */
    public function setSportId(int $sportId): void
    {
        $this->sportId = $sportId;
    }

    /**
     * @return int
     */
    public function getTeamId(): int
    {
        return $this->teamId;
    }

    /**
     * @param int $teamId
     */
    public function setTeamId(int $teamId): void
    {
        $this->teamId = $teamId;
    }

    /**
     * @return int
     */
    public function getCityId(): int
    {
        return $this->cityId;
    }

    /**
     * @param int $cityId
     */
    public function setCityId(int $cityId): void
    {
        $this->cityId = $cityId;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getTeamName(): string
    {
        return $this->teamName;
    }

    /**
     * @param string $teamName
     */
    public function setTeamName(string $teamName): void
    {
        $this->teamName = $teamName;
    }
}
