<?php

namespace ssi\entity\events;

use ssi\entity\match\MatchModel;

class EventsHandler
{

    public function execute()
    {
        $models = $this->parseModels();

        $matches = [];
        /** Bei einigen Sportarten gibt es mehr als 2 Mannschaften (Bowling usw.) */
        foreach ($models as $match) {
            $matches[] = $this->buildMatch($match);
        }

        return $matches;
    }

    /**
     * @param array $event
     * @return EventModel
     */
    private function buildEventModel(array $event): EventModel
    {
        $model = new EventModel();
        $model->setId($event['id']);
        $model->setMatchId($event['match_id']);
        $model->setSportId($event['sport_id']);
        $model->setTeamId($event['team_id']);
        $model->setCityId($event['city_id']);
        $model->setTeamName(
            EventRepository::getTeamNameById($event['id'])
        );
        $model->setDate(
            date('Y-m-d H:i:s', strtotime($event['date']))
        );

        return $model;
    }

    /**
     * @return array
     */
    private function parseModels()
    {
        $events = EventRepository::getAll();

        $eventsModels = [];
        foreach ($events as $eventsGroup) {
            foreach ($eventsGroup as $event) {
                $model = $this->buildEventModel($event);
                /** Parsing für die Modelle nach match_id */
                $eventsModels[$event['match_id']][] = $model;
            }
        }

        return $eventsModels;
    }

    /**
     * @param array $match
     * @return MatchModel
     */
    private function buildMatch(array $match): MatchModel
    {
        $teams = [];
        $cityId = 0;
        $date = '';
        $sportId = 0;
        $matchId = 0;
        /** @var EventModel $model */
        foreach ($match as $model) {
            $teams[] = $model->getTeamName();
            $cityId = $model->getCityId();
            $date = $model->getDate();
            $sportId = $model->getSportId();
            $matchId = $model->getMatchId();
        }

        $match = new MatchModel();
        $match->setCityId($cityId);
        $match->setCityName(
            EventRepository::getCityNameById($cityId)
        );
        $match->setDate($date);
        $match->setSportType(
            EventRepository::getSportTypeById($sportId)
        );
        $match->setSportId($sportId);
        $match->setMatchId($matchId);
        $match->setTeamNames(implode(' - ', $teams));

        return $match;
    }

}
