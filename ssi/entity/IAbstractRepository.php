<?php

namespace ssi\entity;

interface IAbstractRepository
{
    public static function getPagesCount(string $entity);
    public static function getTeamNameById(int $teamId);
    public static function getCityNameById(int $cityId);
    public static function getSportTypeById(string $cityId);
}
