<?php


namespace ssi\template;

use ssi\Config;
use ssi\entity\events\EventsHandler;
use ssi\entity\match\MatchModel;

class IndexHandler
{

    public function execute()
    {
        $kalenderTemplate = file_get_contents(Config::TEMPLATE_DIR . 'index.html');
        $kalenderZeileTemplate = file_get_contents(Config::TEMPLATE_DIR . 'kalenderZeile.html');

        $events = new EventsHandler();
        $matches = $events->execute();
        $table = '';
        /** @var MatchModel $event */
        foreach ($matches as $event) {
            $date = date('D., d.m.Y, H:i', strtotime($event->getDate()));
            $verein = $event->getTeamNames();
            $veranstaltung = $event->getSportType();
            $table .= sprintf($kalenderZeileTemplate, $date, $veranstaltung, $verein);
        }

        return sprintf($kalenderTemplate, $table);
    }
}
