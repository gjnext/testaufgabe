<?php
/**
 * Created by PhpStorm.
 * User: nsidelnikov
 * Date: 23.10.2018
 * Time: 18:39
 */

namespace ssi;

class Config
{
    # ==================================================================================
    #                                        DATABASE INFORMATION
    const DB_NAME = "sportradar";
    const DB_HOST = "localhost";
    const DB_USERNAME = "root";
    const DB_PASSWORD = "";
    # ==================================================================================

    const ROOT_DIR = __DIR__ . '/../';
    const TEMPLATE_DIR = self::ROOT_DIR . 'templates/';

}
