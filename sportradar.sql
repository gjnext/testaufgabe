-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 29 2022 г., 07:52
-- Версия сервера: 8.0.24
-- Версия PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `sportradar`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cities`
--

CREATE TABLE `cities` (
  `id` int NOT NULL,
  `city_name` varchar(50) NOT NULL,
  `land_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Дамп данных таблицы `cities`
--

INSERT INTO `cities` (`id`, `city_name`, `land_id`) VALUES
(1, 'Wien', 1),
(2, 'Salzburg', 1),
(3, 'Graz', 1),
(4, 'Wolfsberg', 1),
(5, 'Dortmund', 2),
(6, 'München', 2),
(7, 'Leverkusen', 2),
(8, 'Leipzig', 2),
(9, 'Rom', 5),
(10, 'Mailand', 5),
(11, 'Turin', 5),
(12, 'Neapel', 5),
(15, 'Madrid', 6),
(16, 'Barcelona', 6),
(17, 'Sevilla', 6),
(18, 'Valencia', 6),
(19, 'Villach', 1),
(20, 'Düsseldorf', 2),
(21, 'Köln', 2),
(22, 'Asiago', 5),
(23, 'Canazei', 5),
(24, 'Bruneck', 5),
(25, 'Wolkenstei', 5),
(26, 'Jaca', 6),
(27, 'Majadahonda', 6),
(28, 'Puigcerdà', 6);

-- --------------------------------------------------------

--
-- Структура таблицы `events`
--

CREATE TABLE `events` (
  `id` int NOT NULL,
  `match_id` int NOT NULL,
  `sport_id` int NOT NULL,
  `team_id` int NOT NULL,
  `city_id` int NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Дамп данных таблицы `events`
--

INSERT INTO `events` (`id`, `match_id`, `sport_id`, `team_id`, `city_id`, `date`) VALUES
(1, 1, 1, 1, 1, '2022-03-31 18:00:00'),
(2, 1, 1, 2, 1, '2022-04-01 17:00:00'),
(3, 2, 1, 3, 2, '2022-04-02 15:00:00'),
(4, 2, 1, 4, 2, '2022-04-03 15:00:00'),
(19, 3, 2, 17, 1, '2022-04-08 16:00:00'),
(20, 3, 2, 18, 1, '2022-04-09 17:00:00'),
(21, 4, 2, 19, 2, '2022-04-15 16:00:00'),
(22, 4, 2, 20, 2, '2022-04-16 16:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `lands`
--

CREATE TABLE `lands` (
  `id` int NOT NULL,
  `land_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Дамп данных таблицы `lands`
--

INSERT INTO `lands` (`id`, `land_name`) VALUES
(1, 'Österreich'),
(2, 'Deutschland'),
(5, 'Italien'),
(6, 'Spanien');

-- --------------------------------------------------------

--
-- Структура таблицы `ligues`
--

CREATE TABLE `ligues` (
  `id` int NOT NULL,
  `ligue_name` varchar(50) NOT NULL,
  `land_id` int NOT NULL,
  `sport_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Дамп данных таблицы `ligues`
--

INSERT INTO `ligues` (`id`, `ligue_name`, `land_id`, `sport_id`) VALUES
(1, 'Österreichische Fußball-Bundesliga', 1, 1),
(2, 'Fußball-Bundesliga', 2, 1),
(3, 'Serie A', 5, 1),
(4, 'Erste Division', 6, 1),
(5, 'ICE Hockey League', 1, 2),
(6, 'Deutsche Eishockey Liga', 2, 2),
(7, 'Serie A', 5, 2),
(8, 'Superliga', 6, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `sports`
--

CREATE TABLE `sports` (
  `id` int NOT NULL,
  `sports_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Дамп данных таблицы `sports`
--

INSERT INTO `sports` (`id`, `sports_name`) VALUES
(1, 'Fußball'),
(2, 'Eishockey');

-- --------------------------------------------------------

--
-- Структура таблицы `teams`
--

CREATE TABLE `teams` (
  `id` int NOT NULL,
  `team_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `city_id` int NOT NULL,
  `ligue_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Дамп данных таблицы `teams`
--

INSERT INTO `teams` (`id`, `team_name`, `city_id`, `ligue_id`) VALUES
(1, 'FK Austria Wien', 1, 1),
(2, 'SK Sturm Graz', 3, 1),
(3, 'FC Red Bull Salzburg', 2, 1),
(4, 'Wolfsberger AC', 4, 1),
(5, 'Borussia Dortmund', 5, 2),
(6, 'FC Bayern München', 6, 2),
(7, 'Bayer 04 Leverkusen', 7, 2),
(8, 'RB Leipzig', 8, 2),
(9, 'AS Rom', 9, 3),
(10, 'AC Mailand', 10, 3),
(11, 'Juventus F.C.', 11, 3),
(12, 'SSC Neapel', 12, 3),
(13, 'Real Madrid', 15, 4),
(14, 'FC Barcelona', 16, 4),
(15, 'FC Sevilla', 17, 4),
(16, 'FC Valencia', 18, 4),
(17, 'Vienna Capitals', 1, 5),
(18, 'EC RB Salzburg', 2, 5),
(19, 'EC Villacher Sportverein', 19, 5),
(20, 'Graz 99ers', 3, 5),
(21, 'EHC RB München', 6, 6),
(22, 'Maddogs München', 6, 6),
(23, 'Düsseldorfer EG', 20, 6),
(24, 'Kölner Haie', 21, 6),
(25, 'Asiago Hockey', 22, 7),
(26, 'SHC Fassa', 23, 7),
(27, 'HC Gherdëina', 25, 7),
(28, 'HC Pustertal', 24, 7),
(29, 'FC Barcelona', 16, 8),
(30, 'CH Jaca', 26, 8),
(31, 'Majadahonda HC', 27, 8),
(32, 'CG Puigcerdà', 28, 8);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `lands`
--
ALTER TABLE `lands`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ligues`
--
ALTER TABLE `ligues`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sports`
--
ALTER TABLE `sports`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблицы `events`
--
ALTER TABLE `events`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT для таблицы `lands`
--
ALTER TABLE `lands`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `ligues`
--
ALTER TABLE `ligues`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `sports`
--
ALTER TABLE `sports`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
