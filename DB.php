<?php

use ssi\Config;

/**
 * Class DB
 */
final class DB
{
    /** @var PDO */
    static public $dbh;

    private static function dbConnect()
    {
        if (!self::$dbh) {

            $dsn = "mysql:dbname=" . Config::DB_NAME . ";host=" . Config::DB_HOST;
            try {
                $dbh = new PDO($dsn, Config::DB_USERNAME, Config::DB_PASSWORD);
            } catch (PDOException $e) {
                throw $e;
            }

            self::$dbh = $dbh;
        }
    }

    /**
     * @param $query
     * @param null $input_parameters
     * @return array
     */
    public static function select($query, $input_parameters = null): array
    {
        self::dbConnect();

        $sth = self::$dbh->prepare($query);
        $sth->execute($input_parameters);

        return $sth->fetchAll();

    }

    /**
     * @param $query
     * @param null $input_parameters
     * @return string
     */
    public static function insert($query, $input_parameters = null): string
    {
        self::dbConnect();

        $sth = self::$dbh->prepare($query);
        $sth->execute($input_parameters);

        return self::$dbh->lastInsertId();

    }

    /**
     * @param $query
     * @param $phantom_id
     * @param $id
     * @param null $input_parameters
     * @return bool
     */
    public static function delete($query, $phantom_id, $id, $input_parameters = null): bool
    {
        self::dbConnect();
        $sth = self::$dbh->prepare($query);
        $sth->bindParam($phantom_id, $id);

        return $sth->execute($input_parameters);
    }

    /**
     * @param $query
     * @param null $input_parameters
     * @return bool
     */
    public static function update($query, $input_parameters = null): bool
    {
        self::dbConnect();
        $sth = self::$dbh->prepare($query);

        return $sth->execute($input_parameters);
    }
}
