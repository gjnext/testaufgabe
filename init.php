<?php

use ssi\template\IndexHandler;

spl_autoload_register(function ($class_name) {
    $class_file = str_replace("\\", DIRECTORY_SEPARATOR, __DIR__ . "\\{$class_name}.php");
    include_once $class_file;
});

$a = new IndexHandler();
$html = $a->execute();

echo $html;